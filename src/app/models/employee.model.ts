export class Employee {
  status?: string;
  data?: EmployeeDetails;
  message?: string;
}

export class EmployeeDetails {
  [key: string]: any;
  id?: any;
  employee_name?: string;
  employee_age?: number;
  employee_salary?: string;
  date?: string;
  sch_in?: string;
  sch_out?: string;
  location!: google.maps.LatLngLiteral;
  att_images?: string;
  profile_images?: string;
}

export class PositionGeo {
  lat!: number;
  lng!: number;
}
