export class ProductDetails {
  [key: string]: any;
  id?: string;
  product_name?: string;
  grade?: string;
  stock!: number;
  sold!: number;
  price!: number;
  product_images?: string;
  product_desc?: string;
}

export class ProductOrder {
  productDetails?: ProductDetails;
  qty!: number;
  total?: string;
}
