export class TrialOne {
  duplicateNumbers?: string[];
  result?: string;
}

export class TrialTwo {
  totalCoins?: number | null;
  totalAmount?: number | null;
  coinFractions?: string | null;
  result?: any;
}

export class TrialThree {
  loopText?: string | null;
  loopNumber?: number | null;
}
