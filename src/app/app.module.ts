import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './pages/employee/list/list.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { DetailComponent } from './pages/employee/detail/detail.component';
import { TrialComponent } from './pages/trial/trial.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberOneComponent } from './pages/trial/number-one/number-one.component';
import { NumberTwoComponent } from './pages/trial/number-two/number-two.component';
import { NumberThreeComponent } from './pages/trial/number-three/number-three.component';
import { NumberFourComponent } from './pages/trial/number-four/number-four.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { GmapsComponent } from './components/gmaps/gmaps.component';
import { WebcamModule } from 'ngx-webcam';
import { WebcamComponent } from './components/webcam/webcam.component';
import { LOAD_WASM, NgxScannerQrcodeModule } from 'ngx-scanner-qrcode';
import { ScannerComponent } from './components/scanner/scanner.component';
import { ProductComponent } from './pages/product/product.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CustomerServiceComponent } from './pages/customer-service/customer-service.component';

LOAD_WASM().subscribe();

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    EmployeeListComponent,
    GmapsComponent,
    NumberOneComponent,
    NumberTwoComponent,
    NumberThreeComponent,
    NumberFourComponent,
    PaginationComponent,
    TrialComponent,
    WebcamComponent,
    ScannerComponent,
    ProductComponent,
    NavbarComponent,
    SidebarComponent,
    CustomerServiceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    GoogleMapsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    NgxScannerQrcodeModule,
    ReactiveFormsModule,
    WebcamModule,
  ],
  exports: [GmapsComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
