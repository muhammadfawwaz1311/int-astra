// gmaps.service.ts

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GmapsService {
  constructor(private httpClient: HttpClient) {}

  getGoogleMaps(): Observable<boolean> {
    return this.httpClient
      .jsonp(
        `${environment.gmaps.url}/js?key=${environment.gmaps.key}`,
        'callback'
      )
      .pipe(
        map(() => true),
        catchError(() => of(false))
      );
  }
}
