// chatgpt.service.ts

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ChatgptService {
  constructor(private httpClient: HttpClient) {}

  private gptHeaders = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${environment.openai.key}`,
    }),
  };

  postMessageGPT(payload: any): Observable<any> {
    return this.httpClient
      .post(`${environment.openai.url}/completions`, payload, this.gptHeaders)
      .pipe(map((res) => res));
  }
}
