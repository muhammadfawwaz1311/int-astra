import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from './pages/employee/list/list.component';
import { TrialComponent } from './pages/trial/trial.component';
import { ProductComponent } from './pages/product/product.component';
import { CustomerServiceComponent } from './pages/customer-service/customer-service.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'employee-list' },
  { path: 'employee-list', component: EmployeeListComponent },
  { path: 'products', component: ProductComponent },
  { path: 'trial', component: TrialComponent },
  { path: 'customer-service', component: CustomerServiceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
