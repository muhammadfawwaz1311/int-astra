// navbar.component.ts

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'comp-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  constructor(private router: Router) {}

  logo = {
    title: 'Angular',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9LvcPj3hSDqVTMGc-1IpxWJoyTIqTkGZt3u95Iu7RTFJ2NM31LrC-zyqZjSx8tKfZlMg&usqp=CAU',
  };
  userLogin = {
    username: 'User',
    userProfile:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS785biEGWYfQ3kCbvts_QRuNPn7IJpvovN4A&usqp=CAU',
  };

  handleRoute(menu: any) {
    this.router.navigateByUrl(menu.id);
  }
}
