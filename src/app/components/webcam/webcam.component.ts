// webcam.component.ts

import { Component, EventEmitter, Output } from '@angular/core';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'comp-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss'],
})
export class WebcamComponent {
  showWebcam: boolean = true;
  isCameraExist: boolean = true;
  allowCameraSwitch: boolean = true;
  deviceId: string = '';
  webcamErrors: WebcamInitError[] = [];
  currentImage?: WebcamImage;

  private trigger: Subject<void> = new Subject<void>();
  private nextWebcam: Subject<boolean | string> = new Subject<
    boolean | string
  >();

  view = {
    height: 300,
    width: 400,
  };

  @Output() getImage = new EventEmitter<WebcamImage>();

  // Check Camera Device Available
  ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs().then((devices: MediaDeviceInfo[]) => {
      this.isCameraExist = devices && devices.length > 0;
    });
  }

  // Handling Image Captured
  onHandleImage(image: WebcamImage): void {
    this.showWebcam = false;
    this.currentImage = image;
    this.getImage.emit(image);
  }

  // Trigger Capture
  onCaptureImage(): void {
    this.trigger.next();
  }

  // Reset Image Captured
  onResetImage(): void {
    this.currentImage = undefined;
    this.showWebcam = true;
  }

  // Toggle On/Of Camera
  toggleWebcam() {
    this.showWebcam = !this.showWebcam;
  }

  // Handling Error Webcam
  onErrorWebcam(error: WebcamInitError): void {
    this.webcamErrors.push(error);
  }

  // Switch Camera (Front/Back)
  onSwitchCamera(directionOrDeviceId: boolean | string) {
    this.nextWebcam.next(directionOrDeviceId);
  }

  // Set Active  Camera Device
  cameraWasSwitched(deviceId: string): void {
    this.deviceId = deviceId;
  }

  get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }
}
