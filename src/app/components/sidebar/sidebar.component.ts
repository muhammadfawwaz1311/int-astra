// sidebar.component.ts

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'comp-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  constructor(private router: Router) {}

  sidebarMenu = [
    {
      id: '/employee-list',
      title: 'Employee',
      icon: 'assets/svg/employee.svg',
    },
    {
      id: '/products',
      title: 'Products',
      icon: 'assets/svg/products.svg',
    },
    {
      id: '/trial',
      title: 'Trial',
      icon: 'assets/svg/trial.svg',
    },
    {
      id: '/customer-service',
      title: 'Customer Service',
      icon: 'assets/svg/cs.svg',
    },
  ];

  handleRoute(menu: any) {
    this.router.navigateByUrl(menu.id);
  }
}
