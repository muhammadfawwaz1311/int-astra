// gmaps.component.ts

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { GmapsService } from 'src/app/services/gmaps/gmaps.service';

@Component({
  selector: 'comp-gmaps',
  templateUrl: './gmaps.component.html',
  styleUrls: ['./gmaps.component.scss'],
})
export class GmapsComponent implements OnInit {
  @Input() gMapsOption: google.maps.MapOptions = {
    center: undefined,
    zoom: 16,
  };
  @Input() markerPositions!: google.maps.LatLngLiteral;
  @Output() getMarker = new EventEmitter<google.maps.LatLngLiteral>();

  gMapsLoaded!: Observable<boolean>;
  markerOptions: google.maps.MarkerOptions = {
    draggable: false,
  };
  view = {
    height: '300px',
    width: '400px',
  };

  constructor(private gmapsService: GmapsService) {}

  // Get Geolocation
  getCoordLocation() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    });
  }

  // Get Latitude & Longitude
  async getLocation(): Promise<google.maps.LatLngLiteral> {
    let currentPosition: google.maps.LatLngLiteral;

    currentPosition = await this.getCoordLocation().then((position: any) => {
      return {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
    });
    return currentPosition;
  }

  // Change Marker Position
  onMoveMarker(event: google.maps.MapMouseEvent) {
    if (event.latLng) {
      this.markerPositions = event.latLng?.toJSON();
      this.getMarker.emit(event.latLng?.toJSON());
    }
  }

  // Set Default Center Map & Marker Position
  ngOnInit(): void {
    if (!this.markerPositions) {
      this.getLocation().then((position) => {
        this.gMapsOption = {
          ...this.gMapsOption,
          center: position,
        };
        this.markerPositions = position;
        this.getMarker.emit(position);
      });
    } else {
      this.gMapsOption = {
        ...this.gMapsOption,
        center: this.markerPositions,
      };
    }

    this.gMapsLoaded = this.gmapsService.getGoogleMaps();
  }
}
