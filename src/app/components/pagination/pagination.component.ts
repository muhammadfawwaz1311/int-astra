import { Component } from '@angular/core';

@Component({
  selector: 'comp-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
  page = 1;
  pageSize = [5, 10, 15];
}
