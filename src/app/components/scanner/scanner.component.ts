// scanner.component.ts

import {
  AfterViewInit,
  Component,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import {
  NgxScannerQrcodeComponent,
  ScannerQRCodeConfig,
  ScannerQRCodeResult,
} from 'ngx-scanner-qrcode';

@Component({
  selector: 'comp-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss'],
})
export class ScannerComponent implements AfterViewInit {
  config: ScannerQRCodeConfig = {
    constraints: {
      video: {
        width: { min: 640, ideal: 1920 },
        height: { min: 360, ideal: 1080 },
        aspectRatio: { ideal: 1.7777777778 },
      },
    },
    isBeep: true,
    vibrate: 300,
  };

  @ViewChild('action') action!: NgxScannerQrcodeComponent;
  @Output() getScanner = new EventEmitter<string>();

  constructor() {}

  // Start QR Scanner
  ngAfterViewInit(): void {
    this.action.isReady.subscribe(() => {
      this.handleScanner(this.action, 'start');
    });
  }

  // Get Scanner Value
  onScan(res: ScannerQRCodeResult[], action?: any): void {
    if (res && res.length) {
      const { value } = res[0];
      value && action && action.stop();
      this.getScanner.emit(value);
    }
  }

  // Handling Scanner Device Env
  handleScanner(action: any, fn: string): void {
    const playDeviceFacingBack = (devices: any[]) => {
      // front camera or back camera check here!
      const device = devices.find((f) =>
        /back|rear|environment/gi.test(f.label)
      ); // Default Back Facing Camera
      action.playDevice(device ? device.deviceId : devices[0].deviceId);
    };

    if (fn === 'start') {
      action[fn](playDeviceFacingBack).subscribe();
    } else {
      action[fn]().subscribe();
    }
  }
}
