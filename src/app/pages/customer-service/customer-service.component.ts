// customer-service.component.ts

import {
  AfterViewChecked,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Chatgpt } from 'src/app/models/chatgpt.model';
import { ChatgptService } from 'src/app/services/chatgpt/chatgpt.service';

@Component({
  selector: 'customer-service',
  templateUrl: './customer-service.component.html',
  styleUrls: ['./customer-service.component.scss'],
})
export class CustomerServiceComponent implements OnInit, AfterViewChecked {
  constructor(
    private formBuilder: FormBuilder,
    private chatgptService: ChatgptService
  ) {}

  @ViewChild('chatScroll') private chatScroll!: ElementRef;

  chatForm = this.formBuilder.group({ role: 'user', content: '' });
  chatQuestion: Chatgpt[] = [];
  chatResponse: Chatgpt[] = [];

  dummyImages = {
    man: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS785biEGWYfQ3kCbvts_QRuNPn7IJpvovN4A&usqp=CAU',
    woman:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4oz0KdCvHj_hvY5exy-qFr06SPFjyA4ZoPg&usqp=CAU',
  };

  ngOnInit(): void {
    this.autoScrollBottom();
  }

  ngAfterViewChecked(): void {
    this.autoScrollBottom();
  }

  autoScrollBottom(): void {
    try {
      this.chatScroll.nativeElement.scrollTop =
        this.chatScroll.nativeElement.scrollHeight;
    } catch (error) {}
  }

  async onSendMessage() {
    let formData = this.chatForm.getRawValue();
    this.chatQuestion.push(formData);
    this.chatResponse.push(formData);

    let payload = {
      model: 'gpt-3.5-turbo',
      messages: this.chatQuestion,
    };

    this.chatForm.reset({ role: 'user' });

    this.chatgptService.postMessageGPT(payload).subscribe({
      next: (res: any) => {
        let { choices } = res;
        choices?.map((choice: any) => {
          this.chatResponse.push(choice?.message);
        });
      },
      error: (err: any) => {},
    });
  }
}
