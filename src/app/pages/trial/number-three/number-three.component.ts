import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TrialThree } from 'src/app/models/trial.model';

@Component({
  selector: 'trial-number-three',
  templateUrl: './number-three.component.html',
  styleUrls: ['./number-three.component.scss'],
})
export class NumberThreeComponent {
  ttValue: TrialThree = {
    loopText: '',
    loopNumber: 0,
  };

  ttForm = this.formBuilder.group({
    loopText: '',
    loopNumber: 0,
  });

  constructor(private formBuilder: FormBuilder) {}

  onSubmitTT(): void {
    this.ttValue = this.ttForm.getRawValue();
    this.ttForm.reset();
  }

  getNumberText(num: number): string {
    let numberText = `${num}`;

    if (num == 0) {
      numberText = '00';
    }
    if (num >= 1 && num < 10) {
      numberText = '0' + num;
    }

    return numberText;
  }
}
