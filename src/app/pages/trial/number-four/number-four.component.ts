import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'trial-number-four',
  templateUrl: './number-four.component.html',
  styleUrls: ['./number-four.component.scss'],
})
export class NumberFourComponent {
  tfValue = {
    inputText: '',
    reverseText: '',
  };

  tfForm = this.formBuilder.group({
    reverseText: '',
  });

  constructor(private formBuilder: FormBuilder) {}

  onSubmitTF(): void {
    this.tfValue = {
      inputText: this.tfForm.getRawValue().reverseText || '',
      reverseText: this.getReverseText(
        this.tfForm.getRawValue().reverseText || ''
      ),
    };
    this.tfForm.reset();
  }

  getReverseText(text: string): string {
    let reversed = '';

    let splitString = text?.split('');
    let reverseArr = splitString.reverse();
    reversed = reverseArr.join('');

    return reversed;
  }
}
