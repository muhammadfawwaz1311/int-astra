import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberFourComponent } from './number-four.component';

describe('NumberFourComponent', () => {
  let component: NumberFourComponent;
  let fixture: ComponentFixture<NumberFourComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NumberFourComponent]
    });
    fixture = TestBed.createComponent(NumberFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
