import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberTwoComponent } from './number-two.component';

describe('NumberTwoComponent', () => {
  let component: NumberTwoComponent;
  let fixture: ComponentFixture<NumberTwoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NumberTwoComponent]
    });
    fixture = TestBed.createComponent(NumberTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
