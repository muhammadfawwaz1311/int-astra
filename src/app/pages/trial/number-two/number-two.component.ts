import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TrialTwo } from 'src/app/models/trial.model';

@Component({
  selector: 'trial-number-two',
  templateUrl: './number-two.component.html',
  styleUrls: ['./number-two.component.scss'],
})
export class NumberTwoComponent {
  twValue: TrialTwo = {
    totalCoins: 0,
    totalAmount: 0,
    coinFractions: '',
    result: '',
  };

  twForm = this.formBuilder.group({
    totalCoins: 0,
    totalAmount: 0,
    coinFractions: '',
  });

  constructor(private formBuilder: FormBuilder) {}

  checkCoinFractions(
    totalCoins: number | null,
    coinFractions: string | null
  ): string {
    if (totalCoins && coinFractions) {
      let fracArr: any[] = coinFractions?.split(',') || [];
      let newFrac = fracArr.slice(0, totalCoins);

      return newFrac.join(',');
    }

    return '';
  }

  getSum(num: number[]): number {
    let sum = num.reduce((a, b) => +a + +b, 0);
    return sum;
  }

  getCombinations(coinsArr: any[]): any[] {
    let combos = coinsArr.reduce((comb, item) => {
      const newComb = comb?.map((combTemp: any[]) => combTemp.concat(item));
      comb.push([item], ...newComb);
      return comb;
    }, []);

    return combos;
  }

  getSmallCombinations(combArr: any[]): number {
    if (combArr.length > 0) {
      let smallComb = combArr.reduce((prev, cur) =>
        prev.length > cur.length ? cur : prev
      );

      return smallComb.length;
    }

    return 0;
  }

  getCoinCombinations(
    totalAmount?: number | null,
    coinFractions?: string | null
  ): number {
    let smallComb = 0;
    let fracArr: any[] = coinFractions?.split(',') || [];

    let chanceArr = this.getCombinations(fracArr).filter(
      (comb) => this.getSum(comb) === totalAmount
    );
    smallComb = this.getSmallCombinations(chanceArr);

    return smallComb;
  }

  onSubmitTW(): void {
    let { totalCoins, totalAmount, coinFractions } = this.twForm.getRawValue();
    this.twValue = {
      ...this.twValue,
      totalCoins: totalCoins,
      totalAmount: totalAmount,
      coinFractions: this.checkCoinFractions(totalCoins, coinFractions),
      result: this.getCoinCombinations(totalAmount, coinFractions),
    };

    this.twForm.reset();
  }
}
