import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberOneComponent } from './number-one.component';

describe('NumberOneComponent', () => {
  let component: NumberOneComponent;
  let fixture: ComponentFixture<NumberOneComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NumberOneComponent]
    });
    fixture = TestBed.createComponent(NumberOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
