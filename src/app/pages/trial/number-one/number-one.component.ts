import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TrialOne } from 'src/app/models/trial.model';

@Component({
  selector: 'trial-number-one',
  templateUrl: './number-one.component.html',
  styleUrls: ['./number-one.component.scss'],
})
export class NumberOneComponent {
  toValue: TrialOne = {
    duplicateNumbers: [],
    result: '',
  };

  toForm = this.formBuilder.group({
    duplicateNumbers: '',
  });

  constructor(private formBuilder: FormBuilder) {}

  onCheckDuplicate(): void {
    const { duplicateNumbers } = this.toValue;
    let counts = {};

    counts = duplicateNumbers?.reduce((prev: any, cur: any) => {
      prev[cur] = (prev[cur] || 0) + 1;
      return prev;
    }, {});

    let arrCount = Object.values(counts);
    let maxCount = Math.max(Number(...arrCount));
    let result: string = Object.keys(counts)
      ?.filter((key) => counts[key as keyof typeof counts] === maxCount)
      .join(',');

    this.toValue = {
      ...this.toValue,
      result,
    };
  }

  onSubmitTO(): void {
    this.toValue = {
      ...this.toValue,
      duplicateNumbers: this.toForm.value?.duplicateNumbers?.split(',') || [],
    };
    this.onCheckDuplicate();
    this.toForm.reset();
  }
}
