// product.component.ts

import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProductDetails, ProductOrder } from 'src/app/models/product.model';

@Component({
  selector: 'products',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers: [DatePipe],
})
export class ProductComponent {
  constructor(private datePipe: DatePipe, private formBuilder: FormBuilder) {}

  currentDate: string | Date | null = this.datePipe.transform(
    new Date(),
    'EEEE, dd MMMM yyyy'
  );

  scanModal: boolean = false;
  orderDetailModal: boolean = false;

  // Table Columns
  columns = [
    {
      id: 'id',
      columnLabel: 'ID',
      class: 'text-left px-2 py-3',
      components: 'text',
    },
    {
      id: 'product_name',
      columnLabel: 'Product Name',
      class: 'text-left px-2 py-3',
      components: 'text',
    },
    {
      id: 'grade',
      columnLabel: 'Grade',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'stock',
      columnLabel: 'Stock',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'sold',
      columnLabel: 'Sold',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'price',
      columnLabel: 'Price',
      class: 'text-right px-2 py-3',
      components: 'currency',
    },
    {
      id: 'product_images',
      columnLabel: 'Product',
      class: 'text-center px-2 py-3',
      components: 'image',
    },
  ];

  // Dummy Product List
  productList: ProductDetails[] = [
    {
      id: 'PRD-001',
      product_name: 'Steak Waroeng',
      grade: 'A',
      stock: 150,
      sold: 0,
      price: 250000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmg2H9-YlGjB1BGPG2f5OEy_hLaI0ZUJtD7Q&usqp=CAU',
      product_desc:
        'Steaks are top of mind for consumers, in fact, 48% of consumers eat a steak on a weekly basis, according to research recently conducted by National Cattlemen’s Beef Association, a contractor to the Beef Checkoff. This research was conducted among 1,500 consumers and sought to understand more about consumer preferences when it comes to dining out for steak.',
    },
    {
      id: 'PRD-002',
      product_name: 'Pizza Hut',
      grade: 'B',
      stock: 100,
      sold: 0,
      price: 100000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZqoeQFGctJ1hrlOFwAbitAUTOGMCJv-INkg&usqp=CAU',
      product_desc:
        'pizza, dish of Italian origin consisting of a flattened disk of bread dough topped with some combination of olive oil, oregano, tomato, olives, mozzarella or other cheese, and many other ingredients, baked quickly—usually, in a commercial setting, using a wood-fired oven heated to a very high temperature—and served hot',
    },
    {
      id: 'PRD-003',
      product_name: 'Burger King',
      grade: 'C',
      stock: 50,
      sold: 0,
      price: 45000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRasqXGJEkxe64O_OG-ZX8NhIFOuA-CNXW9JA&usqp=CAU',
      product_desc:
        'Tasty, delicious, and has me craving more on the first bite.” to “Juicy, mouthwatering, tasty, and everything you’d ever want to savor. There are many kinds of crave worthy foods. From foods that are sweet and salty to savory, there is one crave-able item that sure gets consumers talking. National Cattlemen’s Beef Association, a contractor to the Beef Checkoff, recently conducted research on consumer preferences for burgers when dining out. Consumers’ own words highlight just how enticing burgers are for them.',
    },
    {
      id: 'PRD-004',
      product_name: 'Caesar Salad',
      grade: 'C',
      stock: 90,
      sold: 0,
      price: 25000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtAs66UAeDST4O8ERktdGoKWg4JtwmSS44mg&usqp=CAU',
      product_desc:
        'Caesar salad is made with romaine lettuce, croutons, Parmesan cheese, and Caesar dressing. The dressing is a mixture of olive oil, lemon juice, Worcestershire sauce, Dijon mustard, and garlic.',
    },
    {
      id: 'PRD-005',
      product_name: 'Fried Chicken',
      grade: 'C',
      stock: 50,
      sold: 0,
      price: 50000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSZUCTdzLnBGp3M3k7GzQjyCTCKEFZGdmtDA&usqp=CAU',
      product_desc:
        'Fried chicken has been described as being "crunchy" and "juicy", as well as "crispy". The dish has also been called "spicy" and "salty". Occasionally, fried chicken is also topped with chili like paprika, or hot sauce to give it a spicy taste.',
    },
    {
      id: 'PRD-006',
      product_name: 'Fruit Juice',
      grade: 'C',
      stock: 50,
      sold: 0,
      price: 15000,
      product_images:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFsy8ZbyXjpER2nyEivrkOquaHZy-bWbYbFw&usqp=CAU',
      product_desc:
        'Fruit juice is 100% pure juice made from the flesh of fresh fruit or from whole fruit, depending on the type used. It is not permitted to add sugars, sweeteners, preservatives, flavourings or colourings to fruit juice.',
    },
  ];

  currentOrder: ProductOrder = { qty: 1 };

  // Dummy Image Profile
  dummyImages = {
    angularLogo:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9LvcPj3hSDqVTMGc-1IpxWJoyTIqTkGZt3u95Iu7RTFJ2NM31LrC-zyqZjSx8tKfZlMg&usqp=CAU',
    man: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS785biEGWYfQ3kCbvts_QRuNPn7IJpvovN4A&usqp=CAU',
    woman:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4oz0KdCvHj_hvY5exy-qFr06SPFjyA4ZoPg&usqp=CAU',
    scanner:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSISreNasJbEY4NIVMMuESII4obWIImLi07ug&usqp=CAU',
  };

  // Order Form
  orderForm = this.formBuilder.group({ id: '', qty: 1 });

  // Find Product Based on Scan Value (ID)
  getScanner(id: string) {
    const productDetails =
      this.productList.find((prd) => prd.id == id) || undefined;
    this.currentOrder = {
      ...this.currentOrder,
      productDetails: productDetails,
    };

    this.toggleScanModal(false);
    this.toggleOrderDetailModal(true);
  }

  // Handling Purchase Product
  onPurchaseProduct() {
    const id = this.currentOrder?.productDetails?.id;
    const qty = this.orderForm.getRawValue().qty || 0;

    this.productList = this.productList?.map((prd: ProductDetails) => {
      if (prd.id === id) {
        return {
          ...prd,
          stock: prd.stock > 0 ? prd.stock - qty : prd.stock,
          sold: prd.sold + qty,
        };
      }

      return prd;
    });

    this.toggleOrderDetailModal(false);
  }

  // Convert Number to Currency
  convertCurrency(price?: number): string {
    let tempPrice = price || 0;

    let currency = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    });

    return currency.format(tempPrice);
  }

  // Calculate Total
  getTotalOrder(): string {
    let total = 0;

    if (this.currentOrder.productDetails) {
      const { price } = this.currentOrder?.productDetails;
      const qty = this.orderForm.getRawValue().qty || 1;

      total = price * qty;
    }

    return this.convertCurrency(total);
  }

  // Handling Toggle Scan Modal
  toggleScanModal(bool: boolean) {
    this.scanModal = bool;
  }

  // Handling Toggle Order Detail Modal
  toggleOrderDetailModal(bool: boolean) {
    this.orderForm.reset({ qty: 1 });
    this.orderDetailModal = bool;
  }
}
