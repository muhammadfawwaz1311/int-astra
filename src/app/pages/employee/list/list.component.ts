// list.component.ts

import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { WebcamImage } from 'ngx-webcam';
import { EmployeeDetails } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee/employee.service';

@Component({
  selector: 'employee-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [DatePipe],
})
export class EmployeeListComponent {
  constructor(
    private datePipe: DatePipe,
    private employeeService: EmployeeService,
    private formBuilder: FormBuilder
  ) {}

  currentDate: string | Date | null = this.datePipe.transform(
    new Date(),
    'EEEE, dd MMMM yyyy'
  );

  detailModal: boolean = false;
  checkInModal: boolean = false;

  // Table Columns
  columns = [
    {
      id: 'id',
      columnLabel: 'ID',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'employee_name',
      columnLabel: 'Full Name',
      class: 'text-left px-2 py-3',
      components: 'textImage',
    },
    {
      id: 'employee_age',
      columnLabel: 'Age',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'employee_salary',
      columnLabel: 'Salary',
      class: 'text-right px-2 py-3',
      components: 'text',
    },
    {
      id: 'shift',
      columnLabel: 'Shift',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'check_in',
      columnLabel: 'Check In',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'check_out',
      columnLabel: 'Check Out',
      class: 'text-center px-2 py-3',
      components: 'text',
    },
    {
      id: 'att_images',
      columnLabel: 'Attendance',
      class: 'text-center px-2 py-3',
      components: 'image',
    },
    {
      id: 'action',
      columnLabel: '',
      class: 'text-center px-2 py-3',
      components: 'action',
    },
  ];

  // Dummy Image Profile
  dummyImages = {
    angularLogo:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9LvcPj3hSDqVTMGc-1IpxWJoyTIqTkGZt3u95Iu7RTFJ2NM31LrC-zyqZjSx8tKfZlMg&usqp=CAU',
    man: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS785biEGWYfQ3kCbvts_QRuNPn7IJpvovN4A&usqp=CAU',
    woman:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4oz0KdCvHj_hvY5exy-qFr06SPFjyA4ZoPg&usqp=CAU',
    alt: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLFhJEVjGAVBH-XiKoDa6Ft9NMyqgrY7m86Q&usqp=CAU',
    camera: 'https://cdn-icons-png.flaticon.com/128/45/45010.png',
    out: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkJruL0ZJETrKnZrPOA8MBOa_42xi_YclNJQ&usqp=CAU',
  };

  // Dummy Shift List
  shiftList = [
    { id: 'shift-1', label: '06:00 - 14:00' },
    { id: 'shift-2', label: '14:00 - 22:00' },
    { id: 'shift-3', label: '22:00 - 06:00' },
  ];

  // Dummy Employee List
  employeeList: EmployeeDetails[] = [
    {
      id: 'EMP-1',
      employee_name: 'Tiger Nixon',
      employee_salary: 'Rp. 3.000.000',
      employee_age: 25,
      profile_images: this.dummyImages.man,
      location: { lat: 0, lng: 0 },
    },
    {
      id: 'EMP-2',
      employee_name: 'Garrett Winters',
      employee_salary: 'Rp. 4.000.000',
      employee_age: 28,
      profile_images: this.dummyImages.man,
      location: { lat: 0, lng: 0 },
    },
    {
      id: 'EMP-3',
      employee_name: 'Ashton Cox',
      employee_salary: 'Rp. 5.000.000',
      employee_age: 32,
      profile_images: this.dummyImages.woman,
      location: { lat: 0, lng: 0 },
    },
    {
      id: 'EMP-4',
      employee_name: 'Cedric Kelly',
      employee_salary: 'Rp. 6.000.000',
      employee_age: 35,
      profile_images: this.dummyImages.man,
      location: { lat: 0, lng: 0 },
    },
    {
      id: 'EMP-5',
      employee_name: 'Airi Satou',
      employee_salary: 'Rp. 7.000.000',
      employee_age: 38,
      profile_images: this.dummyImages.woman,
      location: { lat: 0, lng: 0 },
    },
  ];
  employeeDetails?: EmployeeDetails;

  // Check In Form
  checkInForm = this.formBuilder.group({
    id: '',
    shift: '',
    att_images: '',
    location: { lat: 0, lng: 0 },
  });

  getEmployeeList(): void {
    this.employeeService.getEmployeeList().subscribe(({ data }) => {
      this.employeeList = data;
    });
  }

  // Get Image Captured on Webcam
  getImage(image: WebcamImage) {
    this.checkInForm.patchValue({
      att_images: image.imageAsDataUrl,
    });
  }

  // Get Marker Position on Maps
  getMarker(markerPosition: google.maps.LatLngLiteral) {
    this.checkInForm.patchValue({
      location: markerPosition,
    });
  }

  // Submit Check In Form
  onCheckIn(): void {
    const payload = { ...this.checkInForm.value };

    this.employeeList = this.employeeList?.map((emp: EmployeeDetails) => {
      if (emp.id === payload.id)
        return {
          ...emp,
          check_in: this.datePipe.transform(new Date(), 'HH:mm:ss'),
          shift: payload.shift,
          att_images: payload.att_images || '',
          location: payload.location || { lat: 0, lng: 0 },
        };

      return emp;
    });

    this.toggleCheckInModal(false);
  }

  // Submit Check Out
  onCheckOut(id: string): void {
    this.employeeList = this.employeeList?.map((emp: EmployeeDetails) => {
      if (emp.id === id)
        return {
          ...emp,
          check_out: `${this.datePipe.transform(new Date(), 'HH:mm:ss')}`,
        };

      return emp;
    });
  }

  // Handling Detail Modal Data
  onOpenDetail(id: string) {
    this.employeeDetails = this.employeeList.find((emp) => emp.id === id);
    this.toggleDetailModal(true);
  }

  // Handling Toggle Detail Modal
  toggleDetailModal(bool: boolean) {
    this.detailModal = bool;
  }

  // Handling Toggle Check In Form Modal
  toggleCheckInModal(bool: boolean) {
    this.checkInForm.reset();
    this.checkInModal = bool;
  }
}
