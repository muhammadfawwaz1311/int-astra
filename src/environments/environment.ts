export const environment = {
  production: false,
  baseUrl: 'https://dummy.restapiexample.com/api/v1',
  gmaps: {
    url: 'https://maps.googleapis.com/maps/api',
    key: '',
  },
  openai: {
    url: 'https://api.openai.com/v1/chat',
    key: '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',
  },
};
